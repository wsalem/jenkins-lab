# Jenkins Lab

This is Jenkins lab definition used as part of Become a DevOps Jenkins Master course.

## Installation steps (Centos 7):

1. Uninstall old versions:
```
sudo yum remove docker \
docker-client \
docker-client-latest \
docker-common \
docker-latest \
docker-latest-logrotate \
docker-logrotate \
docker-engine
```

2. Install using the repository:
```
sudo yum install -y yum-utils

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

3. Install Docker Engine:
```
sudo yum install docker-ce docker-ce-cli containerd.io
```

4. Run Docker Service:
```
sudo systemctl start docker
```

5. Run this command to download the current stable release of Docker Compose:
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

6. Apply executable permissions to the binary:
```
sudo chmod +x /usr/local/bin/docker-compose
```

7. Change director to repo roota and the following commands:
```aidl
docker-compose build
docker-compose up -d
```